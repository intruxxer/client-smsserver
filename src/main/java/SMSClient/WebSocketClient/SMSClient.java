package SMSClient.WebSocketClient;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.net.URI;
import java.net.URISyntaxException;

public class SMSClient extends WebSocketClient
{
	private static final String ipAddr = "10.0.1.13";
	private static final String portNo = "8080";
    public SMSClient(URI serverURI) { super(serverURI); }

	public static void main( String[] args ) throws URISyntaxException, InterruptedException
    {
        URI server = new URI("ws://" + ipAddr + ":" + portNo);
        SMSClient client = new SMSClient(server);
        //Initializing Connection
        client.connectBlocking();
		System.out.println( SMSClient.class.getName() + " Connecting to Client" );
		//Composing JSON Message
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("no", new JsonPrimitive("081214628696"));
		jsonObject.add("msg", new JsonPrimitive("From: Java SMS Client."));
		//Converting to JSON String
		Gson gson         = new Gson();
		String jsonString = gson.toJson(jsonObject);
		//Sending Message in 'String' to WebSocket Server
		client.send(jsonString);
		System.out.println( SMSClient.class.getName() + ": JSON String: " + jsonString );
		//Immediately Disconnect upon Sending Message is Finished
		client.closeBlocking();
    }
	
	@Override
	public void onOpen(ServerHandshake serverHandshake) {
		System.out.println( SMSClient.class.getName() + ": Connected to Server." );
		
	}

	@Override
	public void onClose(int i, String s, boolean b) {
		System.out.println( SMSClient.class.getName() + ": Disconnected from Server." );
		
	}

	@Override
	public void onError(Exception e) {
		System.out.println( SMSClient.class.getName() + ": Error occurs." + e.getMessage() );
		
	}

	@Override
	public void onMessage(String s) {
		System.out.println( SMSClient.class.getName() + ": Server Message: " + s );
		
	}

}
